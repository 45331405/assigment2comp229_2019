import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.util.*;

public class Stage {
    
    Grid grid;
    ArrayList<Actor> actors;
    List<Cell> cellOverlay;
    List<MenuItem> menuOverlay;
    Optional<Actor> actorInAction;
    List<Cell> actorsPos;
    enum State {ChoosingActor, SelectingNewLocation, CPUMoving, SelectingMenuItem, selectTarget}
    State currentState = State.ChoosingActor;


    
    public Stage(){
        grid = new Grid();
        actors = new ArrayList<Actor>();
        actorsPos = new ArrayList<Cell>();
        cellOverlay = new ArrayList<Cell>();
        menuOverlay = new ArrayList<MenuItem>();
        currentState = State.ChoosingActor;
    }
    public void paint(Graphics g, Point mouseLoc){

        // do we have AI moves to make
        if (currentState == State.CPUMoving){
            
            for(Actor a: actors){
                actorsPos.add(a.loc);
                if (!a.isTeamRed()){
                    List<Cell> possibleLocs = grid.getRadius(a.loc, a.moves, true);
                    possibleLocs.removeAll(actorsPos);
                    Cell nextLoc = a.strat.chooseNextLoc(possibleLocs);
                    a.setLocation(nextLoc);
                }
            }
            currentState = State.ChoosingActor;
            for(Actor a: actors){
                a.turns = 1;
            }
        }
        grid.paint(g,mouseLoc);
        grid.paintOverlay(g, cellOverlay, new Color(0f, 0f, 1f, 0.5f));

        for(Actor a: actors){
            a.paint(g);   
        }
        // state display
        g.setColor(Color.DARK_GRAY);
        g.drawString(currentState.toString(),720,20);

        Optional<Cell> cap = grid.cellAtPoint(mouseLoc);
        if (cap.isPresent()){
            Cell capc = cap.get();
            g.setColor(Color.DARK_GRAY);
            g.drawString(String.valueOf(capc.col) + String.valueOf(capc.row), 720, 50);
            g.drawString(capc.description, 800 , 50);
            g.drawString("Movemtn Cost", 720, 120);
            g.drawString(String.valueOf(capc.moveCost),820,65);
        } 
        // agent display
        int yloc = 138;
        for(int i = 0; i < actors.size(); i++){
            Actor a = actors.get(i);
            g.drawString(a.getClass().toString(),720, yloc + 70*i);
            g.drawString("location:", 730, yloc + 13 + 70 * i);
            g.drawString(Character.toString(a.loc.col) + Integer.toString(a.loc.row), 840, yloc + 13 + 70 * i);
            g.drawString("redness:", 730, yloc + 26 + 70*i);
            g.drawString(Float.toString(a.redness), 840, yloc + 26 + 70*i);
            g.drawString("strat:", 730, yloc + 39 + 70*i);
            g.drawString(a.strat.toString(), 840, yloc + 39 + 70*i);
            g.drawString("Strength:", 730, yloc + 52 + 70*i);
            g.drawString(Float.toString(a.Damage), 840, yloc + 52 + 70*i);
        }

        for(MenuItem mi : menuOverlay){
            mi.paint(g);
        }
    }
    public void mouseClicked(int x, int y){
        switch (currentState) {
            case ChoosingActor:
                // remove empty actor
                actorInAction = Optional.empty();
                // loop for every Actor a in actors list
                for (Actor a : actors) {
                    //if it has the actor has mouse position and the actor is red and it has a turn 
                    if (a.loc.contains(x, y) && a.isTeamRed() && a.turns > 0) {
                        // get all the cell from this location and the amount of its move value around
                        cellOverlay = grid.getRadius(a.loc, a.moves, true);
                        // we make the actor in action == to the actor we just clicked
                        actorInAction = Optional.of(a);
                        // we move locations 
                        currentState = State.SelectingNewLocation;
                    }
                }
                if(!actorInAction.isPresent()){
                    currentState = State.SelectingMenuItem;
                    menuOverlay.add(new MenuItem("Opps", x,y, () -> currentState = State.ChoosingActor));
                    menuOverlay.add(new MenuItem("End Turn" , x, y + MenuItem.height, () -> currentState = State.CPUMoving));
                    menuOverlay.add(new MenuItem("End Game" , x, y+ MenuItem.height * 2, () -> System.exit(0)));
                }
                break;
            case SelectingNewLocation:
                Optional<Cell> clicked = Optional.empty();
                for (Cell c : cellOverlay) {
                    if (c.contains(x, y)) {
                        clicked = Optional.of(c);
                    }
                }
               
                if (clicked.isPresent() && actorInAction.isPresent()) {
                    cellOverlay = new ArrayList<Cell>();
                    actorInAction.get().setLocation(clicked.get());
                    actorInAction.get().turns--;
                    menuOverlay.add(new MenuItem("FIRE" , x,y, () -> {
                        cellOverlay = grid.getRadius(actorInAction.get().loc, actorInAction.get().range,false);
                        cellOverlay.remove(actorInAction.get().loc);
                        currentState = State.selectTarget;
                    }));
                    currentState = State.SelectingMenuItem;
                } 
                break;
            case SelectingMenuItem:
                for(MenuItem mi : menuOverlay){
                    if (mi.contains(x,y)) {
                        mi.action.run();
                        menuOverlay = new ArrayList<MenuItem>();
                    }
                }
                break;
            case selectTarget:
                for(Cell c : cellOverlay){
                    Optional<Actor> oc = actorAt(c);
                    if(oc.isPresent()){
                        oc.get().makeRedder(actorInAction.get().Damage);
                    }
                }
                cellOverlay = new ArrayList<Cell>();
                currentState = State.ChoosingActor;
                break;
            default:
                System.out.println(currentState);
                break;
        }

    }

    public Optional<Actor> actorAt( Cell c){
        for(Actor a : actors){
            if(a.loc == c){
                return Optional.of(a);
            }
        }
        return Optional.empty();
    }

	public void mouseClicked(MouseEvent e) {
	    mouseClicked(e.getX(), e.getY());
	}
}