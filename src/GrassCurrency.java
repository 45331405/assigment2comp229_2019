import java.awt.Color;

public class GrassCurrency implements CostCurrency {

	@Override
    public int MovementCost(Color c){
        return ((c.getGreen() - 100)/50);
    }

}