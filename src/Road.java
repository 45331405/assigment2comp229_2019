import java.awt.Color;
class Road extends Cell{


    
    public Road(char col, int row, int x, int y){
        super(col,row,x,y);
        this.description = "Road";
        color = Color.LIGHT_GRAY;
        this.typeOfCost = new RoadCurrency();
        this.moveCost = typeOfCost.MovementCost(color);
    }
   
}