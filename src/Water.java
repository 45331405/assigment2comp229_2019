import java.awt.Color;
class Water extends Cell{


    
    public Water(char col, int row, int x, int y){
        super(col,row,x,y);
        this.description = "Water";
        color = Color.BLUE;
        this.typeOfCost = new WaterCurrency();
        this.moveCost = typeOfCost.MovementCost(color);
    }

   
}