import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Color;

public class MenuItem extends Rectangle{
    Runnable action;
    String display;
    static int height = 30;
    static int width = 200;


    public MenuItem( String _display, int _x, int _y ,Runnable _action){

        super(_x,_y,width, height);
        this.display = _display;
        this.action = _action;
    }

    public void paint(Graphics g){
        g.setColor(new Color(1.0f,1.0f,1.0f,1.0f));
        g.fillRect(x,y,width,height);
        g.setColor(Color.BLACK);
        g.drawRect(x,y,width, height);
        g.drawRect(x - 1,y - 1,width, height);
        g.drawString(display, x + 8 , y +23);
    }
}