import java.awt.Color;
import java.util.Random;
class Grass extends Cell{
    private static Random rand = new Random();

    public Grass(char col, int row, int x, int y){
        super(col,row,x,y);
        this.description = "GrassLand";
        color = new Color( rand.nextInt(50), rand.nextInt(100)+ 150, rand.nextInt(50));
        this.typeOfCost = new GrassCurrency();
        this.moveCost = typeOfCost.MovementCost(color);
    }
    
    
}